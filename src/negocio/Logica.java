/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.Archivos;
import java.util.ArrayList;

/**
 *
 * @author nati2
 */
public class Logica {

Archivos archivos = new Archivos();

public String addingNewCollaborator(String id,
                                   String name,
                                   String birthdate,
                                   String admissionDate,
                                   String gender,
                                   String department,
                                   String age,
                                   String englishLevel,
                                   String jobTitle){
    return archivos.addingNewCollaborator(id,
                                          name,
                                          birthdate,
                                          admissionDate,
                                          gender,
                                          department,
                                          age,
                                          englishLevel,
                                          jobTitle);
    
}

public ArrayList<String> insertingFile(String filename) {
        return archivos.readingFiles(filename);
    }

public ArrayList<String> readingFiles(String fileName) {
        ArrayList<String> listaUsuarios = archivos.readingFiles(fileName);
        return listaUsuarios;
    }

public ArrayList<String> readingCollabFile(String fileName) {
        ArrayList<String> listaUsuarios = archivos.readingCollabFile(fileName);
        return listaUsuarios;
    }





}
