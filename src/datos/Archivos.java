/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.swing.JOptionPane;
import objetos.Colaborador;

/**
 *
 * @author nati2
 */
public class Archivos {
    
    Colaborador newColaborador = new Colaborador();

    public String addingNewCollaborator(String id,
                                   String name,
                                   String birthdate,
                                   String admissionDate,
                                   String gender,
                                   String department,
                                   String age,
                                   String englishLevel,
                                   String jobTitle) {
        
        
        File collaboratorsFile = new File("collaborators.txt");
        
        String result = "";
        
        newColaborador.setId(id);
        newColaborador.setName(name);
        newColaborador.setBirthday(birthdate);
        newColaborador.setAdmissionDate(admissionDate);
        newColaborador.setGender(gender);
        newColaborador.setDepartment(department);
        newColaborador.setAge(age);
        newColaborador.setEnglishLevel(englishLevel);
        newColaborador.setJobTitle(jobTitle);
        
        String info = newColaborador.getCollaboratorDetails();
        
        try {
            collaboratorsFile.createNewFile();
            result = "Persona añadida satisfactoriamente.";
        } catch (Exception ex) {
            System.out.println("Error al crear el archivo." + ex);
        }

        try {
            BufferedWriter collaborators = new BufferedWriter(new FileWriter(collaboratorsFile, true));
            collaborators.write(info + "\r\n");
            collaborators.close();
        } catch (Exception ex) {
            System.out.println("Error al crear el archivo." + ex);
        }

        return result;

    }
    
    public ArrayList<String> readingFiles(String fileName) {
        File fileInstance = new File(fileName);
        ArrayList<String> fileContent = new ArrayList<>();
        ArrayList<String> valuesFiltered = new ArrayList<>();
        try {
            BufferedReader file = new BufferedReader(new FileReader(fileInstance));
            while (file.ready()) {
                fileContent.add(file.readLine());
            }
            for (String content : fileContent) {               
                String[] tempArray = content.split(",", 0);
                valuesFiltered.add(tempArray[0]);
            }
            file.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return valuesFiltered;
    }
    
    public ArrayList<String> readingCollabFile(String fileName) {
        File fileInstance = new File(fileName);
        ArrayList<String> fileContent = new ArrayList<>();
        try {
            BufferedReader file = new BufferedReader(new FileReader(fileInstance));
            while (file.ready()) {
                fileContent.add(file.readLine());
            }
            
            file.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: " + fileName + ".txt\n" + ex);
        }
        return fileContent;
    }
    
    
  

}
