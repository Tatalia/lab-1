/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.util.ArrayList;

/**
 *
 * @author nati2
 */
public class Colaborador {
    
    private String id;
    private String name;
    private String gender;
    private String department;
    private String birthday;
    private String age;
    private String admissionDate;
    private String englishLevel;
    private String jobTitle;

       
    //public static ArrayList collaboratorList = new ArrayList<>();
    
    public Colaborador(String id, String name, String gender, String department, String birthday, String age, String admissionDate, String englishLevel, String jobTitle) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.department = department;
        this.birthday = birthday;
        this.age = age;
        this.admissionDate = admissionDate;
        this.englishLevel = englishLevel;
        this.jobTitle = jobTitle;
    }

    public Colaborador() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(String admissionDate) {
        this.admissionDate = admissionDate;
    }

    public String getEnglishLevel() {
        return englishLevel;
    }

    public void setEnglishLevel(String englishLevel) {
        this.englishLevel = englishLevel;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
    
    public String getCollaboratorDetails() {
        String temp = "%s,%s,%s,%s,%s,%s,%s,%s";
        return String.format(temp, getId(), getName(), getGender(), getDepartment(), getBirthday(), getAge(), getAdmissionDate(), getEnglishLevel(), getJobTitle());
    }
    
}
